// TODO: Implement experession parser/lexer/evaluator
// PATTERN: pair(a,b)
// VALUE: swap(pair(swap(pair(a,b)),pair(q,c)))
// Application of rule swap(pair(a,b)) => pair(b,a) to swap(pair(swap(pair(a,b)),pair(q,c))) yields pair(pair(q,c),swap(pair(a,b)))
// test tests::rule_apply_all ... ok

pub mod main {
    use std::any::Any;
    use std::collections::HashMap;
    use std::fmt::{self, write};
    use std::iter::{zip, Peekable};
    use std::ops::Add;
    use std::path::Iter;
    use std::{fmt::Display, mem::swap, vec};

    #[derive(Debug, Clone, PartialEq)]
    pub enum Expr {
        Sym(String),
        Fun(String, Vec<Expr>),
    }

    #[derive(Debug)]
    pub struct Rule {
        pub head: Expr,
        pub body: Expr,
    }


    impl Expr {
        pub fn parse_peekable(mut lexer: &mut Peekable<impl Iterator<Item = Token>>) -> Self {
             if let Some(x) = lexer.next() {
                match x.ttype {
                    TokenType::IDENT => {
                        if let Some(val) = lexer.next_if(|x| x.ttype == TokenType::LPAREN) {
                            // let args = Expr::parse_fun_args(&lexer);
                            let mut args: Vec<Expr> = Vec::new();
                            if let Some(kval) = lexer.next_if(|x| x.ttype == TokenType::RPAREN ) {
                                return Expr::Fun(x.literal, args);
                            }
                            args.push(Expr::parse_peekable(lexer));
                            while let Some(qval) = lexer.next_if(|x| x.ttype == TokenType::COMMA) {
                                args.push(Expr::parse_peekable(lexer));
                            }
                            return Expr::Fun(x.literal, args); 
                        } else {
                            return Expr::Sym(x.literal);
                        }
                    },
                    _ => todo!("IT IS NOT SUPPOSED TO REACH HERE")
                    
                }
             }
             todo!()
        }
        pub fn parse(mut lexer: impl Iterator<Item= Token> ) -> Self { 
            Expr::parse_peekable(&mut lexer.peekable())
        }
    }

    // impl Display {

    // }
    // swap(pair(a,b)) = pair(b,a)

    impl fmt::Display for Expr {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            match self {
                Expr::Sym(a) => write!(f, "{}", a),
                Expr::Fun(name, args) => {
                    write!(f, "{}(", name);
                    for (i, arg) in args.iter().enumerate() {
                        if i > 0 {
                            write!(f, ",");
                        }
                        write!(f, "{}", arg);
                    }
                    return write!(f, ")");
                }
            }
        }
    }

    impl fmt::Display for Rule {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            match self {
                Rule { head, body } => write!(f, "{} => {}", head, body),
            }
        }
    }

    pub type Bindings = HashMap<String, Expr>;

    pub fn pattern_match_impl(pattern: &Expr, expr: &Expr, binding: &mut Bindings) -> bool {
        match (pattern, expr) {
            (Expr::Sym(a), _) => {
                match binding.get(a) {
                    None => {
                        binding.insert(a.clone(), expr.clone());
                        return true;
                    }
                    Some(val) => {
                        return val == expr;
                    }
                }
                // true
            }
            (Expr::Fun(name1, args1), Expr::Fun(name2, args2)) => {
                // println!("This is name1 {} and name2 {}", name1, name2);
                if !(name1 == name2) {
                    return false;
                }
                if args1.len() != args2.len() {
                    return false;
                }
                for (arg1, arg2) in zip(args1, args2) {
                    // println!("This is name1 {} and name2 {}", arg1, arg2);
                    pattern_match_impl(arg1, arg2, binding);
                }
                true
            }
            _ => false,
        }
    }

    pub fn pattern_match(pattern: &Expr, value: &Expr) -> Option<Bindings> {
        let mut binding: Bindings = HashMap::new();
        if pattern_match_impl(pattern, value, &mut binding) {
            Some(binding)
        } else {
            None
        }
    }

    // swap(pair(a,b)) => pair(b,a)

    pub fn transform_exp(bindings: Bindings, expr: &Expr) -> Expr {
        match expr {
            Expr::Sym(a) => {
                if let Some(val) = bindings.get(a) {
                    val.clone()
                } else {
                    panic!("This is not supposed to happend")
                }
            }
            Expr::Fun(name, args) => {
                let mut new_args = Vec::new();
                for arg in args {
                    match arg {
                        Expr::Sym(a) => {
                            if let Some(val) = bindings.get(a) {
                                new_args.push(val.clone());
                            } else {
                                panic!("ERRRRORRR")
                            }
                        }
                        _ => panic!("You cant have an expr pointing to smth in the bindings"),
                    }
                }
                return Expr::Fun(name.to_string(), new_args);
            }
        }
    }

    impl Rule {
        pub fn apply_all(&self, expr: &Expr) -> Expr {
            if let Some(bindings) = pattern_match(&self.head, &expr) {
                let transform = transform_exp(bindings, &self.body);
                println!(
                    "Application of rule {} to {} yields {} ",
                    self, expr, transform
                );
                transform
            } else {
                match expr {
                    Expr::Sym(a) => Expr::Sym(a.to_string()),
                    Expr::Fun(name, args) => {
                        let mut new_args: Vec<Expr> = Vec::new();
                        for arg in args {
                            new_args.push(self.apply_all(arg));
                        }
                        let new_expr = Expr::Fun(name.to_string(), new_args);
                        new_expr
                    }
                }
            }
        }
    }

    // fun(a,b)
    #[derive(Debug, PartialEq, Eq)]
    pub enum TokenType {
        IDENT,
        LPAREN,
        RPAREN,
        COMMA,
        EQAUL,
    }

    #[derive(Debug, PartialEq, Eq)]
    pub struct Token {
        pub ttype: TokenType,
        pub literal: String,
    }

    pub struct Lexer<T: Iterator> {
        text: Peekable<T>,
    }

    impl<Chars: Iterator<Item = char>> Lexer<Chars> {
        pub fn from_iter(chars: Chars) -> Self {
            Self {
                text: chars.peekable(),
            }
        }
    }

    impl<Chars: Iterator<Item = char>> Iterator for Lexer<Chars> {
        type Item = Token;
        fn next(&mut self) -> Option<Self::Item> {
            while let Some(x) = self.text.next_if(|x| x.is_whitespace()) {}

            if let Some(x) = self.text.next() {
                use TokenType::*;
                match x {
                    '(' => Some(Token {
                        ttype: LPAREN,
                        literal: x.to_string(),
                    }),
                    ',' => Some(Token {
                        ttype: COMMA,
                        literal: x.to_string(),
                    }),
                    ')' => Some(Token {
                        ttype: RPAREN,
                        literal: x.to_string(),
                    }),
                    '=' => Some(Token {
                        ttype: EQAUL,
                        literal: x.to_string(),
                    }),
                    _ => {
                        let mut intr = String::new();
                        intr.push(x);
                        if !x.is_alphanumeric() {
                            panic!("Unexpected character {}", x)
                        }
                        while let Some(x) = self.text.next_if(|k| k.is_alphanumeric()) {
                            intr.push(x);
                        }
                        Some(Token {
                            ttype: IDENT,
                            literal: intr,
                        })
                    }
                }
            } else {
                None
            }
        }
    }
}

mod tests {
    // use std::mem::swap;

    use crate::main::*;
    use Expr::*;
    use Lexer;
    use Token;
    use TokenType::*;

    #[cfg(test)]
    use super::*;

    // TODO: MACROS!!

    // fun
    // fun(name, args)
    // Sym(name)

    macro_rules! fun_args {
        () => {vec![]};
        ($arg:ident) => {vec![expr!($arg)]};
        ($arg1:ident,$($rest:tt)*) => {
            {
                let mut args: Vec<Expr> = vec![expr!($arg1)];
                let mut return_val = fun_args!($($rest)*);
                args.append(&mut return_val);
                args
            }
        };
        ($name:ident($($args:tt)*)) => {
            vec![Expr::Fun(stringify!($name).to_string(), fun_args!($($args)*))]
        };
        ($name:ident($($args:tt)*),$($rest:tt)*) => {
            // Expr::Fun(stringify!($name).to_string(), fun_args!($($args)*))
            {
                let mut args: Vec<Expr> = vec![Expr::Fun(stringify!($name).to_string(), fun_args!($($args)*))];
                let mut return_val = fun_args!($($rest)*);
                args.append(&mut return_val);
                args

            }
        }
    }
    macro_rules! expr {
        () => {};
        ($name:ident) => {
            Sym(stringify!($name).to_string())
        };
        ($name:ident($($args:tt)*)) => {
            Expr::Fun(stringify!($name).to_string(), fun_args!($($args)*))
        }
    }

    #[test]

    pub fn parse_test() {
        let example_string = "argv(fun(q),b,df)".chars();
        let mut lexer = Lexer::from_iter(example_string);
        // let val: Vec<_> = (lexer).collect();
        let parsed_expr = Expr::parse(lexer);
        println!("This is the parsed expr {}", parsed_expr);
        // println!("{:?}", val);
        // let test = Token {};
        let expected = vec![
            Token {
                ttype: IDENT,
                literal: "fun".to_string(),
            },
            Token {
                ttype: LPAREN,
                literal: "(".to_string(),
            },
            Token {
                ttype: IDENT,
                literal: "a".to_string(),
            },
            Token {
                ttype: COMMA,
                literal: ",".to_string(),
            },
            Token {
                ttype: IDENT,
                literal: "b".to_string(),
            },
            Token {
                ttype: RPAREN,
                literal: ")".to_string(),
            },
        ];
        // assert!((val == expected));
    }

    pub fn macro_test() {
        println!("THIS should test my macro works {} ", expr!(arg1));

        println!("THIS should test my macro works {} ", expr!(f()));
        println!("THIS should test my macro works {} ", expr!(f(xzgv, b(k))));
        //
        assert!(true);
    }

    #[test]
    pub fn rule_apply_all() {
        let expr1 = expr!(swap(pair(a, b)));

        let expr2 = expr!(pair(b, a));

        let pattern = expr!(pair(b, a));

        let func_body = expr!(swap(pair(swap(pair(a, b)), pair(q, c))));

        let swap_rule = Rule {
            head: expr1.clone(),
            body: expr2.clone(),
        };

        let result = pattern_match(&pattern, &func_body);

        println!("PATTERN: {}", pattern);
        println!("VALUE: {}", func_body);
        ("{}", swap_rule.apply_all(&func_body));

        // if let Some(bindings) = pattern_match(&pattern, &func_body) {
        //     println!("MATCH");
        //     // for (k, v) in bindings.iter() {
        //     //     println!("{} => {} ", k, v);
        //     // }
        //     println!("NO MATCH");
        // }
    }
}

fn main() {}
